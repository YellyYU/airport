#include <iostream>
#include <math.h>
#include <time.h>
#include <fstream>
#include "Server.hpp"

double regularArrivalRate = 10;
double precheckArrivalRate = 143.459;
double scanRate = 122.861;
double AProcessTime = 114.0;
double AEVP = 28.109, BEVP = 58.0;
int AServerNum = 11, BServerNum = 36;

int main() {
	srand((unsigned int)time(NULL));
	double arrivalLambda_regular = 1.0 / regularArrivalRate;
	double arrivalLambda_pre = 1.0 / precheckArrivalRate;
	double AProcessLambda = 1.0 / AProcessTime;
	//ExponentialGenerator *AProcessGen = new ExponentialGenerator(AProcessLambda);
	PoissonGenerator *AProcessGen = new PoissonGenerator(AProcessTime);
	double BProcessLambda = 1.0 / scanRate;
	PoissonGenerator *BProcessGen = new PoissonGenerator(scanRate);
	Server serverA(AServerNum, AProcessGen), serverB(BServerNum, BProcessGen);
	PoissonGenerator *arrivalGen = new PoissonGenerator(regularArrivalRate);
	vector<Passenger> dayout;
	int timeForNextArrival = 0;
	int arriveNum = 1;
	double genResult;
	Queue q1, q2;
	time day = 10 * 3600 * 3; //assuming that delaying more than 3 hrs would cause uncatch of flight
										//arrive rate at most could be 10 people/sec
										//interval is 100ms
	time i = 0;
	do {
		if (timeForNextArrival == 0) {
			vector<Passenger> arrivals;
			for (int cnt = 0; cnt < arriveNum; cnt++) {
				Passenger ap = new _Passenger();
				ap->ts.push_back(i);
				arrivals.push_back(ap);
			}
			q1.pushAll(arrivals);
			timeForNextArrival = arrivalGen->gen();
		}
		q2.pushAll(serverA.stepForward(i, q1));
		vector<Passenger> outs = serverB.stepForward(i, q2);
		vector<Passenger>::iterator endit = outs.end(), it;
		for (it = outs.begin(); it != endit; it++) dayout.push_back(*it);
		timeForNextArrival--;
		i++;
	} while (i < day);

	std::fstream fs;
	fs.open("result.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	if (!fs.is_open()) {
		std::cout << "Error opening file" << std::endl;
		int x; std::cin >> x;
	}
	fs << "No" << "\t" << "arrive" << "\t" << "enterA" << "\t" << "quitA" << "\t" << "enterB" << "\t" << "quitB" << std::endl;
	vector<Passenger>::iterator endit = dayout.end(), it;
	int no = 1;
	for (it = dayout.begin(); it != endit; it++, no++)  {
		fs << no;
		vector<time_t>::iterator it_time = (*it)->ts.begin();
		for (int cnt = 0; cnt < 5; cnt++, it_time++) {
			fs << "\t" << *it_time;
		}
		/*fs << "\t" << (*it)->ts[1]- (*it)->ts[0];
		fs << "\t" << (*it)->ts[2] - (*it)->ts[1];*/
		fs << std::endl;

	}
	fs.close();
	return 0;
}