#pragma once
#include <vector>
#include <random>
using std::vector;
using std::default_random_engine;
using std::exponential_distribution;
using std::poisson_distribution;
using std::normal_distribution;


class Generator {
public:
	virtual int gen() = 0;
};

class PoissonGenerator :public Generator {
private: 
	default_random_engine engine;
	poisson_distribution<int> pois;
public:
	PoissonGenerator(double lambda):pois(lambda) {
		
	}
	int gen() {
		return pois(engine);
	}
};

class ExponentialGenerator :public Generator {
private:
	default_random_engine engine;
	exponential_distribution<double> exp;
public:
	ExponentialGenerator(double lambda) :exp(lambda) {

	}
	int gen() {
		return round(exp(engine));
	}
};

class NormalGenerator :public Generator {
private:
	default_random_engine engine;
	normal_distribution<double> normal;
public:
	NormalGenerator(double mu, double variance) :normal(mu, variance) {

	}
	int gen() {
		return round(normal(engine));
	}
};