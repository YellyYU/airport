#pragma once
#include "Passenger.h"
#include "Queue.hpp"
#include "Generator.hpp"

//remaining time generator
class Server {
private:
	vector<Passenger> passengers;
	vector<time> remaining; //abstract, long long int to simulate actual time
	Generator* gen;
	int sz;
public:
	Server(int size, Generator* g):passengers(size),remaining(size) {
		this->sz = size;
		this->gen = g;
	}
	vector<Passenger> stepForward(time enter, Queue& wait) {
		vector<Passenger> out;
		for (int i = 0;i < sz;i++) {
			if (!remaining[i] && passengers[i]) {
				out.push_back(passengers[i]);
				passengers[i] = nullptr;
			}
			if (passengers[i] == nullptr && !wait.is_empty()) {
				passengers[i] = wait.pop();
				//passengers[i]->ts.push_back(enter);
				int next = gen->gen();
				passengers[i]->ts.push_back(enter + next);
				remaining[i] = next;
			}
			--remaining[i];
		}
		return out;
	}
};