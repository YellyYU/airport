#pragma once
#include "Passenger.h"
#include <vector>
#define CAPACITY 4096
using std::vector;

class Queue {
private:
	Passenger q[CAPACITY];
	int hd,tl;
public:
	Queue(){
		hd = tl = 0;
	}
	void pushAll(const vector<Passenger>& passengers) {
		vector<Passenger>::const_iterator it,ed;
		ed = passengers.end();
		for (it = passengers.begin();it != ed;it++) {
			q[tl] = *it;
			tl = (tl + 1) % CAPACITY;
			if (hd == tl)
				throw 1;//Full
		}
	}
	vector<Passenger> popN(int n) {
		vector<Passenger> r;
		int c=hd-tl;
		c = (c < 0) ? CAPACITY - c : c;
		n = (c < n) ? c : n;
		for (int i = 0;i < n;i++)
			r.push_back(q[(hd + i) % CAPACITY]);
		hd = (hd + n) % CAPACITY;
		return r;
	}
	Passenger pop() {
		if (hd == tl) return NULL;
		int tmp = hd;
		hd = (hd + 1) % CAPACITY;
		return q[tmp];
	}
	bool is_empty() { return (tl == hd) ? true : false; }
};